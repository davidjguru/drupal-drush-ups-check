# Checking Remote Sites' Update Status

This is a simple bash script accompanied by a drush alias file for checking remote Drupal sites' update status.

> This solution is only tested with sites Drupal 7 and 8 that are **not** using dependency management with composer.
>
> This solution is only for Drush 8.

## Requirements

1. Drush 8
2. Remote shell access to the server(s) via SSH key

## Environment Info

This script is test in an environment below:

```bash
Distributor ID:	Ubuntu
Description:	Ubuntu 18.04.2 LTS
Release:	18.04
Codename:	bionic
```

```bash
Drush Version   :  8.1.16
```

```bash
PHP 7.2.15-0ubuntu0.18.04.1 (cli) (built: Feb  8 2019 14:54:22) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.2.15-0ubuntu0.18.04.1, Copyright (c) 1999-2018, by Zend Technologies
    with Xdebug v2.6.0, Copyright (c) 2002-2018, by Derick Rethans
```

## Usage

> Don't forget to backup your sites prior to updates.
>
> Preferably never update your sites directly on the server, neither dev, staging nor live.
>
> First apply the updates on a local copy of each site, test the updates and then do it on the server.
>
> Before making the update, enable maintenance mode.
>
> Ideally work with a SCM system like git, and use a git workflow to maintain your sites.
>
> You won't regret! :)

1. Clone the repo
2. copy `example.livesite.aliases.drushrc.php` as `livesite.aliases.drushrc.php`
3. Configure your aliases
4. Copy `example.drupal_drush_ups_live_alias_names.txt` as `drupal_drush_ups_live_alias_names.txt`
5. Enter all your alias names in this file (`drupal_drush_ups_live_alias_names.txt`) as a line for each.
6. Run `bash drupal_drush_ups_live.sh`
7. After script execution finishes, you will get a list of updates in a csv file under `./files/` folder.