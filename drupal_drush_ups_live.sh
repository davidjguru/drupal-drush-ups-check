#!/bin/bash
# must be run like this
# bash script.sh

cp livesite.aliases.drushrc.php $HOME/.drush/livesite.aliases.drushrc.php
drush cc drush

mydate=$(date +%Y%m%d_%H%M%S)

# Creating aliases array with values coming from the text file.
mapfile -t aliases < drupal_drush_ups_live_alias_names.txt


touch ./files/$mydate-livesites-updates.csv
echo "SiteName,Date,ModulName,OldVersion,NewVersion,Status" >> ./files/$mydate-livesites-updates.csv

for item in ${aliases[*]}
do
  # 7z a ./upload_zip/site_$mydate.zip ./site/
  echo "$item is being checked..."
  drush @$item ups --format=csv > ./files/tmp/$item-$mydate.csv
  sed -i '/Kullanılabilir güncelleme/d' ./files/tmp/$item-$mydate.csv
  sed -i '/güncelleme verisi kontrol ediliyor /d' ./files/tmp/$item-$mydate.csv
  sed -i '/Checking/d' ./files/tmp/$item-$mydate.csv
  sed -i '/No release history was found for the requested project/d' ./files/tmp/$item-$mydate.csv
  sed -i '/Node no longer exists/d' ./files/tmp/$item-$mydate.csv
  sed -i -e "s/^/$item,$mydate,/" ./files/tmp/$item-$mydate.csv
  cat  ./files/tmp/$item-$mydate.csv >> ./files/$mydate-livesites-updates.csv
  #rm ./files/tmp/$item-$mydate.csv
done

# Delete files older than 7 days except .gitkeep files
find ./files* -type f -mtime +7 -not -name '.gitkeep' -delete